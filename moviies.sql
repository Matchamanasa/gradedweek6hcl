create database Mydatabase;
use Mydatabase;



create table Movies_Coming(ID int,
Title varchar(20),
Year int,
Category varchar(20));


create table Movies_inTheatres
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);

create table Top_RatedIndia
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


create table Top_RatedMovies
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


insert into Movies_Coming values(1,'KGF2',2022,'Movies Coming');
insert into Movies_Coming values(2,'SVP',2022,'Movies Coming');
insert into Movies_coming values(3,'Acharya',2022,'Movies Comming');

insert into Movies_inTheatres values(1,'Spriderman',2018,'Movies in Theatres');
insert into Movies_inTheatres values(2,'Infinity War',2018,'Movies in Theatres');
insert into Movies_inTheatres values(3,'End game',2019,'Movies in Theatres');


insert into Top_RatedIndia values(1,'Mayabazar',1971,'Top Rated India');
insert into Top_RatedIndia values(2,'Dangal',2016,'Top Rated India');
insert into Top_RatedIndia values(3,'Drishyam 2',2013,'Top Rated India');

insert into Top_RatedMovies values(1,'Baazigar',1993,'Top Rated Movies');
insert into Top_RatedMovies values(2,'24',2016,'Top Rated Movies');
insert into Top_RatedMovies values(3,'Jodhaa Akbar',2008,'Top Rated Movies');


select*from Movies_Coming;
select*from Top_RatedIndia;
select*from Top_RatedMovies;
select*from Movies_inTheatres;